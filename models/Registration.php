<?php

namespace app\models;

use Yii;
use yii\base\Model;
use app\models\User;

/**
 * RegistrationForm is the model behind the login form.
 */
class Registration extends Model
{
    public $nickName;
    public $email;
    public $password;
    public $passwordConfirm;

    private $_user = false;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['password','nickName','email','passwordConfirm'], 'required'],
            [['passwordConfirm'], 'compare','compareAttribute' => 'password'],
            ['email', 'email'],
        ];
    }

    public function find() {
        return User::find();
    }

    public function register()
    {
        if ($this->validate()) {
            $user = new User();
            $user->setAttributes($this->getAttributes());
            if($user->save()) {
                return true;
            } else {
                $this->duplicateErrorsFromModel($user);
            }
        }

        return false;
    }

    protected function duplicateErrorsFromModel(\yii\base\Model $model)
    {
        foreach ($model->getErrors() as $attribute => $errors) {
            if($this->hasProperty($attribute)) {
                foreach ($errors as $error) {
                    $this->addError($attribute,$error);
                }
            }
        }
    }
}