<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "UserTour".
 *
 * @property integer $userId
 * @property string $email
 * @property string $nickName
 * @property string $firstName
 * @property string $lastName
 * @property string $password
 * @property string $authKey
 * @property string $accessToken
 * @property string $about
 * @property string $photo
 */
class User extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'UserTour';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email', 'password',], 'required'],
            [['about'], 'string'],
            [['email'], 'unique'],
            [['email', 'nickName', 'firstName', 'lastName', 'authKey', 'accessToken', 'photo'], 'string', 'max' => 255],
            [['password'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'userId' => Yii::t('app', 'User ID'),
            'email' => Yii::t('app', 'Email'),
            'nickName' => Yii::t('app', 'Nick Name'),
            'firstName' => Yii::t('app', 'First Name'),
            'lastName' => Yii::t('app', 'Last Name'),
            'password' => Yii::t('app', 'Password'),
            'authKey' => Yii::t('app', 'Auth Key'),
            'accessToken' => Yii::t('app', 'Access Token'),
            'about' => Yii::t('app', 'About'),
            'photo' => Yii::t('app', 'Photo'),
        ];
    }
    public static function findIdentity($id)
    {
        return self::find()->where(['userId' => $id])->one();
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return self::find()->where(['accessToken' => $token])->one();
    }

    /**
     * Finds user by username
     *
     * @param  string      $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return self::find()->where(['email' => $username])->one();
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->userId;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param  string  $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return $this->password === $password;
    }

    public function beforeValidate()
    {
        if(isset($this->password)) {
            // $this->password = Yii::$app->getSecurity()->generatePasswordHash($this->password);
            $this->password = ($this->password);
        }
        if(!$this->about) {
            $this->about = ' ';
        }
        if(!$this->authKey) {
            $this->authKey = ' ';
        }
        if(!$this->accessToken) {
            $this->accessToken = ' ';
        }
        return parent::beforeValidate();
    }
}
