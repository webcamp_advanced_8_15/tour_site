<?php

use yii\db\Schema;
use yii\db\Migration;

class m150729_084056_createUserTourTable extends Migration
{
    public function up()
    {
        $this->createTable('UserTour', [
            'userId' => Schema::TYPE_PK,
            'email' => Schema::TYPE_STRING,
            'nickName' => Schema::TYPE_STRING,
            'firstName' => Schema::TYPE_STRING,
            'lastName' => Schema::TYPE_STRING,
            'password' => Schema::TYPE_STRING,
            'authKey' => Schema::TYPE_STRING,
            'accessToken' => Schema::TYPE_STRING,
            'about' => Schema::TYPE_TEXT,
            'photo' => Schema::TYPE_STRING,
        ]);
    }

    public function down()
    {
        $this->dropTable('UserTour');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
